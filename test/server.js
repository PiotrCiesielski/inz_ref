var http = require('http'),
    url  = require('url'),
    path = require('path'),
    fs   = require('fs');
var mimeTypes = {
    "html": "text/html",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "png": "image/png",
    "js": "text/javascript",
    "css": "text/css"};

var server;

var start = function( spec ) {
  server = http.createServer(function(req, res) {
    var uri      = url.parse(req.url).pathname;
    var filename = path.join(spec.root, uri);
    fs.exists(filename, function(exists) {
      if(!exists) {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write('404 Not Found\n');
        res.end();
        return;
      }
      var mimeType = mimeTypes[path.extname(filename).split(".")[1]];
      res.writeHead(200, {'Content-Type': mimeType});

      var fileStream = fs.createReadStream(filename);
      fileStream.pipe(res);

    });

  });

  server.listen(spec.port);
};

exports.start = start;

var stop = function( ) {
  if ( server !== undefined ) {
    server.close();
    server = undefined;
  }
};

exports.stop = stop;


