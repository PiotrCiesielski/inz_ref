// ------------------------------------------------------------------
// Private functions

// Are the weights known?
// -> true/false
var weightsKnown = (solution) => {
  return (! solution.hasOwnProperty('weights')) || (solution.weights.length > 0);
};

// Returns all the data, for all the operations.
var operations = (problem, solution) => {
  let ops = problem.operations.map( (op) => {
    let o = {
      id: op.id,
      time: op.time,
      assigned: false
    };

    if ( op.preds )
      o.preds = op.preds;
    else
      o.preds = [];

    return o;
  });

  solution.stations.forEach( (st) => {
    st.forEach( (op) => {
      ops.filter( (o) => o.id === op.id )[0].assigned = true;
    });
  });

  if ( weightsKnown(solution) )
    solution.weights.forEach( (wop) => {
      ops.filter( (o) => o.id === wop.id )[0].weight = wop.weight;
    });

  return ops;
};

// Returns the ids of all assigned operations.
var assignedOperations = (solution) => {
  return solution.stations.reduce( (acc, st) => {
    let ids = st.map( (op) => op.id );
    return acc.concat(ids);
  }, [] );
};

// Not a perfect implementaion...
var allOperationsAssigned = (problem, solution) => {
  let assigned_ops = assignedOperations(solution),
      all_ops = problem.operations.map( (op) => op.id );

  return assigned_ops.length === all_ops.length;
};

// Not a perfect implementaion...
var solutionComplete = (problem, solution) => {
  return (solution.indices.hasOwnProperty('le')) &&
         (solution.indices.hasOwnProperty('si')) &&
         (solution.indices.hasOwnProperty('lt'));
};

var operationAssigned = (operations, id) => {
  return operations.filter( (op) => op.id === id )[0].assigned;
};

var operationsPossible = (cycle, stations, operations) => {
  let k                 = stations.length,
      last_station      = stations[k-1],
      st                = Math.max.apply( null, last_station.map( (o) => o.end ) ),
      rem_time          = cycle - st;

  // time constraints
  let ops = operations.filter( (op) => (op.assigned === false) && (op.time <= rem_time) )
  // precedence constraints
                      .filter( (op) => op.preds.every( (id) => operationAssigned(operations, id) ) );

  if ( ops.length === 0 )
    return ops;

  let best_weight = Math.max.apply( null, ops.map( (op) => op.weight ) );

  return ops.filter( (op) => op.weight === best_weight ); 
};

//1) Dla każdej operacji wyznacz listę bezpośrednich następników.
//2) Dla tych operacji, które nie mają żadnych bezpośrednich następników, jako wartości wag przyjmij ich czasy.
//3) Znajdź operację, która:
//  - nie ma wartości wagi
//  - wszyscy jej bezpośredni następnicy mają wartości wag
//4) Jeśli nie ma takich operacji, to znaczy, że wyznaczono już wagi wszystkich operacji. Koniec.
//5) Wagą dla takiej operacji jest
//  time(op) + max weight(follower_op)
//Wróć do punktu 3.
var rpwWeights = (problem) => {
  // the accessors
  let ops = problem.operations.map( (op) => {
    return {
      id: op.id,
      time: op.time,
      preds: op.preds ? op.preds : [],
      accs: []
    };
  });

  ops.forEach( (op) => {
    op.preds.forEach( (pred) => {
      ops.filter( (o) => o.id === pred )[0].accs.push(op.id)
    });
  });

  // no accessors => weight = time
  ops.filter( (op) => op.accs.length === 0 )
     .forEach( (op) => op.weight = op.time );

  let weightKnown = (id) => {
    return ops.filter( (o) => o.id === id )[0].hasOwnProperty('weight');
  };

  while ( true ) {
    // an op with no weight, while all its accessors have weights
    let op = ops.filter( (op) => op.accs.every( weightKnown ) )
                .filter( (op) => ! op.hasOwnProperty('weight') );

    if ( op.length === 0 )
      break;

    op = op[0];

    // calculate the weight
    op.weight = op.time + Math.max.apply( null, ops.filter( (o) => op.accs.indexOf(o.id) !== -1 )
                                                   .map( (o) => o.weight ) );
  }

  return ops.map( (op) => {
    return {
      id:     op.id,
      weight: op.weight
    };
  });
};

var weightsDec = (method, values) => {
  return {
    type: 'weights',
    method, values
  };
};

// prepare the weights decisions
var weightsDecisions = (problem) => {
  let rpw = rpwWeights(problem);

  return [
    weightsDec('rpw', rpw)
  ];
};

var newStationDecision = () => {
  return {
    type: 'new-station'
  };
};

var opDecision = (id) => {
  return {
    type: 'operation', id
  };
};

var operationsDecisions = (ops) => {
  return ops.map( (op) => opDecision(op.id) );
};

var indexDecision = (prop, val) => {
  return { type: prop, value: val };
};

// Efektywność linii
var lineEfficiency = (stations,cycle) => {
	
// LE = suma czasów wykorzystanych w cyklach/(ilość cykli * długość cyklu) * 100%	
// LE dąży do 100%	
  var Arr1 = [];
  let Sum_of_Station_Time =0;
  let x = stations.length;
  for(var i=0; i<x; i++) {
  let y = stations[i];
    for(var j=0;j<1;j++) {
      let Station_time = Math.max.apply( null, y.map( (o) => o.end ) );
      Sum_of_Station_Time += Station_time;
    };
};
let LE_not_rounded = (Sum_of_Station_Time/(x*cycle))*100;
let LE_rounded = parseFloat(LE_not_rounded).toFixed(0);
return LE_rounded;
};

// Współczynnik gładkości
var smoothnessIndex = (stations,cycle) => {

// SI= sqrt(suma kwadratów czasów niewykorzystanych w cyklach)
// SI dąży do 0
  var Arr2 = [];
  let Sum_of_Remaining_Time = 0;

  
  let x1 = stations.length;
  for(var i=0; i<x1; i++) {
  let y1 = stations[i];
    for(var j=0;j<1;j++) {
      let Station_time = Math.max.apply( null, y1.map( (o) => o.end ) );
	  let Remaining_time = cycle - Station_time;
      let Remaining_time_power2 = Math.pow(Remaining_time,2);
	  Sum_of_Remaining_Time += Remaining_time_power2;
    };
};
let SI_not_rounded = Math.sqrt(Sum_of_Remaining_Time);
let SI_rounded = parseFloat(SI_not_rounded).toFixed(2);
return SI_rounded;
};
// Czas linii
var timeOfTheLine = (stations, cycle) => {
	
// LT = ilośc cykli * czas cyklu	
	let k = stations.length,
	 c = cycle,
	 LT = k*c;
	return LT;
};

var indicesDecisions = (cycle, solution) => {
  let decs = [],
      indices = [ 
        ['le', lineEfficiency],
        ['si', smoothnessIndex],
        ['lt', timeOfTheLine]
      ];

  indices.forEach( (x) => {
    let [prop, func] = x;
    if ( solution.indices.hasOwnProperty(prop) )
      return;
    let val = func(solution.stations, cycle);
    decs.push( indexDecision( prop, val ) );
  });

  return decs;
};

var noDecisions = () => {
  return { type: 'nothing-to-do' };
};

// ------------------------------------------------------------------
// Public functions

var decisions = function( problem, solution ) {
  let ops = operations( problem, solution );

  // when weights are not known, it's the beginning
  if ( ! weightsKnown(solution) )
    return weightsDecisions(problem);

  // when the solution is complete, nothing more can be done
  if ( solutionComplete(problem, solution) )
    return [ noDecisions() ];

  // when the full solution is known, it's time to calculate the indices
  if ( ops.every( (op) => op.assigned === true ) )
    return indicesDecisions(problem.cycle, solution);

  // otherwise, calculate the next decisions
  let possible_ops = operationsPossible(problem.cycle, solution.stations, ops);
  if ( possible_ops.length > 0 )
    return operationsDecisions(possible_ops);
  else
    return [ newStationDecision() ];
};

exports.decisions = decisions;